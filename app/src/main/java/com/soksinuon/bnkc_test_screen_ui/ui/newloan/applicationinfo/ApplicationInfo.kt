package com.soksinuon.bnkc_test_screen_ui.ui.newloan.applicationinfo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.soksinuon.bnkc_test_screen_ui.R

class ApplicationInfo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_application_info)
    }
}